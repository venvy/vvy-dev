# Setup #

1. install VirtualBox
2. install Vagrant
3. clone this repository (git clone https://bitbucket.org/venvy/vvy-dev)
4. cd vvy-dev
5. git clone https://bitbucket.org/venvy/vvy-eva (Rename `vvy-eva` folder to `venvy-eva` after clone)
6. execute `vagrant up` in this directory
7. add entry to /etc/hosts (on windows: C:\Windows\System32\drivers\etc\hosts): `10.0.0.101 dev.ecli.ps`
8. open browser and go to https://dev.ecli.ps

# Overview #

Vagrant will provision a server for development, including all dependencies. All application logs are located in `venvy-eva/log`. `access.log` and `error.log` are nginx files, while `d-eclips.log` is the node.js console.log output. Node Supervisor is used to restart node in case of a file change, or in case of a server crash. Careful: a syntax error in Node code (or some other error causing a fatal crash) will leave Supervisor constantly restarting the server until the error is fixed. You should not have to SSH into the dev server normally, it is configured to start on startup automatically.

# Vagrant Basics #

To start dev server >> `vagrant up`
To restart dev server >> `vagrant reload`  
To access dev server >> `vagrant ssh`  
To suspend dev server >> `vagrant suspend`  
To destroy dev server >> `vagrant destroy` (you will lose all changes, push all changes to bitbucket)


Once inside the dev server...

To start node.js >> `start d-eclips`  
To stop node.js >> `stop d-eclips`  
To view log >> `tail -f ~/local/venvy-eva/log/d-eclips.log` (Alternatively, if on windows, use SnakeTail)  
To restart nginx >> `service nginx restart`  
To diagnose issues with nginx config >> `nginx -t`  


# Vagrant Share #


Vagrant can be temporarily shared to the Internet (either for testing, or to remotely show a dev version of the site to someone else) using the `vagrant share` command. The following command should be executed from the same location where you ran `vagrant up`:

`vagrant share --name NAMEHERE --https 443`

Replace NAMEHERE with one of the following:

- eclips-test-serge
- eclips-test-dan
- eclips-test-sam

These are the names that are configured to work in dev nginx. After the share command is successful, you may access the shared server by going to:

`https://NAMEHERE.vagrantshare.com/`

Remember to change app URLs to new shared URL! e.g. inside `venvy-eva/source/config/constants.js`, change `CONST_GLOBAL` url to `NAMEHERE.vagrantshare.com`

If the sharing process unexpectedly stops working, you will have to manually clear the name for reuse. Navigate to https://atlas.hashicorp.com/vagrant and login using the following credentials:

- Email: `serge@ecli.ps`
- Password: [our usual password!]

Find the name you want to clear in the "Shares" box, and clear it. After that, execute the share command again, and you should be good to go!

Original readme by Luke Bayas, updated by Serge Gmyria