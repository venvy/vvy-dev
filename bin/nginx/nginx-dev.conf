#user nobody;
worker_processes  1;

events {
  worker_connections  1024;
}

http {
  include       mime.types;
  default_type  application/octet-stream;

  sendfile        on;
  #tcp_nopush     on;
  #keepalive_timeout  0;
  keepalive_timeout  65;

  #gzip  on;

  gzip  on;
  gzip_disable "MSIE [1-6]\.(?!.*SV1)";
  gzip_http_version 1.1;
  gzip_comp_level 6;
  gzip_proxied any;
  gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;
  gzip_buffers 16 8k;
  gzip_vary on;

  # Dev (local) server
  server {
    listen                  80;
    server_name             dev.ecli.ps;
    rewrite                 ^ https://dev.ecli.ps$request_uri? permanent;
  }

  # Production server
  server {
    listen                  80;
    server_name             test.ecli.ps;
    rewrite                 ^ https://dev.ecli.ps$request_uri? permanent;
  }
  
  # Localhost too
  server {
    listen                  80;
    server_name             localhost;
    rewrite                 ^ https://dev.ecli.ps$request_uri? permanent;
  }

  # Temp Serge's URL here!
  server {
    listen                  443 ssl;
    server_name             eclips-test-serge eclips-test-serge.vagrantshare.com;
    
    ssl_certificate         /home/vagrant/ssl/dev.full.pem;
    ssl_certificate_key     /home/vagrant/ssl/dev.key;
    ssl_protocols           TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers             HIGH:!aNULL:!MD5;

    set                     $rootDir /var/www/venvy-eva;
    root                    $rootDir/public/;
    
    access_log              /var/www/venvy-eva/log/access-temp.log;
    error_log               /var/www/venvy-eva/log/error-temp.log;
    
    

    set $shouldRedirect 0;

    if ($http_user_agent ~ facebook ) {
      set $shouldRedirect 1;
    }

    if ( $request_uri ~ shareable ) {
      set $shouldRedirect 0;
    }

    if ( $shouldRedirect = 1 ) {
      rewrite     ^(.*)   http://eclips-test-serge.vagrantshare.com/api/shareable$1 permanent;
    }
    
    if ($query_string ~ "^(.*)version=(.*)$") {
        rewrite ^(.*)$ $uri? permanent;
    }
    
    location /ext {
      try_files $uri$args $uri$args/ /ext.html;
    }

    location / {
      try_files $uri$args $uri$args/ /index.html;
    }

    # staging venvy-api
    location /api {
      add_header              'Access-Control-Allow-Origin' '*';
      add_header              'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, PUT, PATCH, DELETE';
      add_header              'Access-Control-Allow-Credentials' 'true';
      add_header              'Access-Control-Allow-Headers' 'X-Requested-With,content-type';
      proxy_redirect          off;
      proxy_http_version      1.1;
      proxy_set_header        Host $http_host;
      proxy_set_header        X-Scheme $scheme;
      proxy_set_header        X-Real-IP $remote_addr;

      proxy_set_header        Upgrade     $http_upgrade;
      proxy_set_header        Connection  "upgrade";

      proxy_hide_header       X-Powered-By;
      proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass              http://localhost:3003;
    }
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   html;
    }
  }
  
  # Temp Dan's URL here!
  server {
    listen                  443 ssl;
    server_name             eclips-test-dan eclips-test-dan.vagrantshare.com;
    
    ssl_certificate         /home/vagrant/ssl/dev.full.pem;
    ssl_certificate_key     /home/vagrant/ssl/dev.key;
    ssl_protocols           TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers             HIGH:!aNULL:!MD5;

    set                     $rootDir /var/www/venvy-eva;
    root                    $rootDir/public/;
    
    access_log              /var/www/venvy-eva/log/access-temp.log;
    error_log               /var/www/venvy-eva/log/error-temp.log;
    
    

    set $shouldRedirect 0;

    if ($http_user_agent ~ facebook ) {
      set $shouldRedirect 1;
    }

    if ( $request_uri ~ shareable ) {
      set $shouldRedirect 0;
    }

    if ( $shouldRedirect = 1 ) {
      rewrite     ^(.*)   http://eclips-test-dan.vagrantshare.com/api/shareable$1 permanent;
    }
    
    if ($query_string ~ "^(.*)version=(.*)$") {
        rewrite ^(.*)$ $uri? permanent;
    }
    
    location /ext {
      try_files $uri$args $uri$args/ /ext.html;
    }

    location / {
      try_files $uri$args $uri$args/ /index.html;
    }

    # staging venvy-api
    location /api {
      add_header              'Access-Control-Allow-Origin' '*';
      add_header              'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, PUT, PATCH, DELETE';
      add_header              'Access-Control-Allow-Credentials' 'true';
      add_header              'Access-Control-Allow-Headers' 'X-Requested-With,content-type';
      proxy_redirect          off;
      proxy_http_version      1.1;
      proxy_set_header        Host $http_host;
      proxy_set_header        X-Scheme $scheme;
      proxy_set_header        X-Real-IP $remote_addr;

      proxy_set_header        Upgrade     $http_upgrade;
      proxy_set_header        Connection  "upgrade";

      proxy_hide_header       X-Powered-By;
      proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass              http://localhost:3003;
    }
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   html;
    }
  }

  # Temp URL here!
  server {
    listen                  443 ssl;
    server_name             eclips-test-sam eclips-test-sam.vagrantshare.com;
    
    ssl_certificate         /home/vagrant/ssl/dev.full.pem;
    ssl_certificate_key     /home/vagrant/ssl/dev.key;
    ssl_protocols           TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers             HIGH:!aNULL:!MD5;

    set                     $rootDir /var/www/venvy-eva;
    root                    $rootDir/public/;
    
    access_log              /var/www/venvy-eva/log/access-temp.log;
    error_log               /var/www/venvy-eva/log/error-temp.log;
    
    

    set $shouldRedirect 0;

    if ($http_user_agent ~ facebook ) {
      set $shouldRedirect 1;
    }

    if ( $request_uri ~ shareable ) {
      set $shouldRedirect 0;
    }

    if ( $shouldRedirect = 1 ) {
      rewrite     ^(.*)   http://eclips-test-sam.vagrantshare.com/api/shareable$1 permanent;
    }
    
    if ($query_string ~ "^(.*)version=(.*)$") {
        rewrite ^(.*)$ $uri? permanent;
    }
    
    location /ext {
      try_files $uri$args $uri$args/ /ext.html;
    }

    location / {
      try_files $uri$args $uri$args/ /index.html;
    }

    # staging venvy-api
    location /api {
      add_header              'Access-Control-Allow-Origin' '*';
      add_header              'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, PUT, PATCH, DELETE';
      add_header              'Access-Control-Allow-Credentials' 'true';
      add_header              'Access-Control-Allow-Headers' 'X-Requested-With,content-type';
      proxy_redirect          off;
      proxy_http_version      1.1;
      proxy_set_header        Host $http_host;
      proxy_set_header        X-Scheme $scheme;
      proxy_set_header        X-Real-IP $remote_addr;

      proxy_set_header        Upgrade     $http_upgrade;
      proxy_set_header        Connection  "upgrade";

      proxy_hide_header       X-Powered-By;
      proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass              http://localhost:3003;
    }
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   html;
    }
  }
  
  # Dev config
  server {

    listen                  443 ssl;
    server_name             dev.ecli.ps;

    ssl_certificate         /home/vagrant/ssl/dev.full.pem;
    ssl_certificate_key     /home/vagrant/ssl/dev.key;
    ssl_protocols           TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers             HIGH:!aNULL:!MD5;

    set                     $rootDir /var/www/venvy-eva;
    root                    $rootDir/public;
    
    access_log              /var/www/venvy-eva/log/access.log;
    error_log               /var/www/venvy-eva/log/error.log;
    

    #Note: for curly braces( { and } ), as they are used both in regexes and for block control, to avoid conflicts, regexes with curly braces are to be enclosed with double quotes (or single quotes).
    #$1: Path before first period.
    #$2: Extension, not including period. Manually added in the second argument. 
    rewrite ^(.*)\.\d\d\d\.(.*)$ $1.$2? last;

    set $shouldRedirect 0;

    if ($http_user_agent ~ facebook ) {
      set $shouldRedirect 1;
    }

    if ( $request_uri ~ shareable ) {
      set $shouldRedirect 0;
    }

    if ( $shouldRedirect = 1 ) {
      rewrite     ^(.*)   https://dev.ecli.ps/api/shareable$1 permanent;
    }
    
    if ($query_string ~ "^(.*)version=(.*)$") {
        rewrite ^(.*)$ $uri? permanent;
    }
    
    location /ext {
      try_files $uri$args $uri$args/ /ext.html;
    }

    location / {
        add_header Cache-Control "private";
        add_header Cache-Control "private, must-revalidate, proxy-revalidate";

        try_files $uri @prerender;
    }

    location ~ \.min\.js$ {
        expires 2d;
        add_header Cache-Control "public";
        add_header Cache-Control "public, must-revalidate, proxy-revalidate";
    }

    location ~* \.(css|js|html)$ {
        expires 2s;
        add_header Cache-Control "private";
        add_header Cache-Control "private, must-revalidate, proxy-revalidate";
    }

    location ~* \.(?:ico|gif|jpe?g|png|svg)$ {
        expires 2s;
        add_header Cache-Control "private";
        add_header Cache-Control "private, must-revalidate, proxy-revalidate";
    }

    location @prerender {
        proxy_set_header X-Prerender-Token szv43XxmYB0CE3GFFh2H;

        set $prerender 0;
        if ($http_user_agent ~* "baiduspider|twitterbot|facebookexternalhit|rogerbot|linkedinbot|embedly|quora link preview|showyoubot|outbrain|pinterest|slackbot|vkShare|W3C_Validator") {
            set $prerender 1;
        }
        if ($args ~ "_escaped_fragment_") {
            set $prerender 1;
        }
        if ($http_user_agent ~ "Prerender") {
            set $prerender 0;
        }
        if ($uri ~ "\.(js|css|xml|less|png|jpg|jpeg|gif|pdf|doc|txt|ico|rss|zip|mp3|rar|exe|wmv|doc|avi|ppt|mpg|mpeg|tif|wav|mov|psd|ai|xls|mp4|m4a|swf|dat|dmg|iso|flv|m4v|torrent|ttf|woff)") {
            set $prerender 0;
        }

        #resolve using Google's DNS server to force DNS resolution and prevent caching of IPs
        resolver 8.8.8.8;

        if ($prerender = 1) {

            #setting prerender as a variable forces DNS resolution since nginx caches IPs and doesnt play well with load balancing
            set $prerender "service.prerender.io";
            rewrite .* /$scheme://$host$request_uri? break;
            proxy_pass http://$prerender;
        }
        if ($prerender = 0) {
            #do not add http headers here
            rewrite .* /index.html break;
        }
    }

    # staging venvy-api
    location /api {
      expires                 2s;
      add_header              Cache-Control "no-cache";
      add_header              Cache-Control "no-cache, no-store, must-revalidate, proxy-revalidate";
      add_header              'Access-Control-Allow-Origin' '*';
      add_header              'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, PUT, PATCH, DELETE';
      add_header              'Access-Control-Allow-Credentials' 'true';
      add_header              'Access-Control-Allow-Headers' 'X-Requested-With,content-type';
      proxy_redirect          off;
      proxy_http_version      1.1;
      proxy_set_header        Host $http_host;
      proxy_set_header        X-Scheme $scheme;
      proxy_set_header        X-Real-IP $remote_addr;

      proxy_set_header        Upgrade     $http_upgrade;
      proxy_set_header        Connection  "upgrade";

      proxy_hide_header       X-Powered-By;
      proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_pass              http://localhost:3003;
    }
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   html;
    }
  }

}
