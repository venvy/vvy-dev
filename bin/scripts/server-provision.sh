#!/bin/sh
sudo su -

HOME=/home/ubuntu

# http://docs.mongodb.org/manual/tutorial/install-mongodb-on-ubuntu/
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
#echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list

# general dependencies
# mongodb-org
apt-get -y update
apt-get -y install nodejs nodejs-legacy npm nginx emacs vim git-core ufw sysv-rc-conf dos2unix

# ssh keys
# cat $HOME/local/bin/ssh/id_dsa.pub >> /home/vagrant/.ssh/authorized_keys
# cp $HOME/local/bin/ssh/id_dsa /home/vagrant/.ssh

# add bitbucket.org to known_hosts
# ssh-keyscan -t rsa bitbucket.org >> /home/vagrant/.ssh/known_hosts

# clone venvy repositories
# cd $HOME/local
# git clone ssh://git@bitbucket.org/venvy/venvy-api.git
# git clone ssh://git@bitbucket.org/venvy/venvy-web.git
# git clone ssh://git@bitbucket.org/venvy/venvy-iva.git
# git clone ssh://git@bitbucket.org/venvy/venvy-merlin.git
# cd $HOME

# link venvy repositories
# Note: must pull to venvy-eva-staging and venvy-eva-production first!
mkdir -p /var/www
ln -s $HOME/local/venvy-eva-staging /var/www
ln -s $HOME/local/venvy-eva-production /var/www

# make log and pid folders/files for staging
cd $HOME/local/venvy-eva-staging/
mkdir log pid
touch log/s-eclips.log pid/s-eclips.pid
chmod +x log/s-eclips.log pid/s-eclips.pid

# ... for production
cd $HOME/local/venvy-eva-production/
mkdir log pid
touch log/p-eclips.log pid/p-eclips.pid
chmod +x log/p-eclips.log pid/p-eclips.pid

# import ssl keys
cd $HOME
mkdir ssl
cp -a $HOME/local/bin/ssl/. $HOME/ssl

# install venvy-api node dependencies
cd /var/www/venvy-eva-staging
npm install

cd /var/www/venvy-eva-production
npm install forever -g
npm install

cd $HOME

# grab upstart scripts

# staging
cp /var/www/venvy-eva/bin/upstart/s-eclips.conf /etc/init/
chmod +x /etc/init/s-eclips.conf
dos2unix /etc/init/s-eclips.conf

# production
cp /var/www/venvy-eva/bin/upstart/p-eclips.conf /etc/init/
chmod +x /etc/init/p-eclips.conf
dos2unix /etc/init/p-eclips.conf

start p-eclips
start s-eclips

# link nginx.conf
mv /etc/nginx/nginx.conf /etc/nginx/nginx.original.conf
ln -s $HOME/local/bin/nginx/nginx-prod.conf /etc/nginx/nginx.conf

# set nginx to start on bootup
sysv-rc-conf nginx on

# restart nginx
service nginx restart

# firewall
sudo ufw enable
sudo ufw allow ssh
sudo ufw allow http
sudo ufw allow https