#!/bin/sh
sudo su -

HOME=/home/vagrant

# http://docs.mongodb.org/manual/tutorial/install-mongodb-on-ubuntu/
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10


# Installing nvm
wget -qO- https://raw.github.com/creationix/nvm/master/install.sh | sh

# This enables NVM without a logout/login
export NVM_DIR="/home/vagrant/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

# Install a node and alias
nvm install 4.2.1
nvm alias default 4.2.1


#echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list

# general dependencies
# mongodb-org
apt-get -y update
apt-get -y install npm nginx emacs vim git-core ufw sysv-rc-conf dos2unix

# ssh keys
# cat $HOME/local/bin/ssh/id_dsa.pub >> /home/vagrant/.ssh/authorized_keys
# cp $HOME/local/bin/ssh/id_dsa /home/vagrant/.ssh

# add bitbucket.org to known_hosts
# ssh-keyscan -t rsa bitbucket.org >> /home/vagrant/.ssh/known_hosts

# clone venvy repositories
# cd $HOME/local
# git clone ssh://git@bitbucket.org/venvy/venvy-api.git
# git clone ssh://git@bitbucket.org/venvy/venvy-web.git
# git clone ssh://git@bitbucket.org/venvy/venvy-iva.git
# git clone ssh://git@bitbucket.org/venvy/venvy-merlin.git
# cd $HOME

# link venvy repositories
mkdir -p /var/www
ln -s $HOME/local/venvy-eva /var/www

# make log and pid folders/files
cd $HOME/local/venvy-eva/
mkdir log pid
touch log/d-eclips.log log/access.log log/error.log pid/d-eclips.pid
chmod +x log/d-eclips.log pid/d-eclips.pid

# import ssl keys
cd $HOME
mkdir ssl
cp -a $HOME/local/bin/ssl/. $HOME/ssl

# install venvy-api node dependencies
cd /var/www/venvy-eva
npm install supervisor -g
npm install

cd $HOME

# grab upstart script

# development
cp /var/www/venvy-eva/bin/upstart/d-eclips.conf /etc/init/
chmod +x /etc/init/d-eclips.conf
dos2unix /etc/init/d-eclips.conf

start d-eclips



# link nginx.conf
mv /etc/nginx/nginx.conf /etc/nginx/nginx.original.conf
ln -s $HOME/local/bin/nginx/nginx-dev.conf /etc/nginx/nginx.conf

# set nginx to start on bootup
sysv-rc-conf nginx on

# restart nginx
service nginx restart

# firewall
sudo ufw enable
sudo ufw allow ssh
sudo ufw allow http
sudo ufw allow https